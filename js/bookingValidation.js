$(document).ready(function(){

    $("#bookingform").validate({

       rules:{

            date_start:{
                require: true,
                date: true,
            },

            date_end:{
                require: true,
                date: true,
            },        

            email:{
                required: true,
                email: true,
            },
            
            phone:{
            },
            
            name:{
                
            },
            
       },

//       messages:{
//
//            login:{
//                required: "Это поле обязательно для заполнения",
//                minlength: "Логин должен быть минимум 4 символа",
//                maxlength: "Максимальное число символо - 16",
//            },
//
//            pswd:{
//                required: "Это поле обязательно для заполнения",
//                minlength: "Пароль должен быть минимум 6 символа",
//                maxlength: "Пароль должен быть максимум 16 символов",
//            },
//
//       }

    });

});
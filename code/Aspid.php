<?php

final class Aspid
{
    /**
     * @var array 
     */
    protected static $_config;
    /**
     * @var \Aspid_Db 
     */
    protected static $_db;
    /**
     * @var array 
     */
    protected static $_modules;
    /**
     * @var \Aspid_Request
     */
    protected static $_request;
     /**
     * @var \Aspid_Response 
     */
    protected static $_response;
     /**
     * @var \Aspid_Route 
     */
    protected static $_route;
    
    
    /**
     * MAIN FUNCTION
     */
    public static function run()
    {
        self::_initialize();
        Aspid::getRoute()
            ->getController()
             ->dispatch();
    }
    
    protected static function _initialize()
    {
        self::_initializeAutoloader();
        self::_initializeConfig();
        self::_initializeDatabase();
        self::_initializeModules();
    }

    protected static function _initializeConfig()
    {
        $config = array();        
        include SITE_ROOT . '/config/config.php';
        self::$_config = $config;
    }

    protected static function _initializeDatabase()
    {
        self::$_db = new Aspid_Db(self::$_config['database']);
    }

    protected static function _initializeModules()
    {
        foreach (glob(SITE_ROOT . '/code/modules/*/*/[M]odule.php') as $path)
        {
            $className = preg_replace('/.*?\/(\w+)\/(\w+)\/(\w+)\.php$/', '$1_$2_$3', $path);
            $moduleId = preg_replace('/.*?\/(\w+)\/(\w+)\/\w+\.php$/', '$1_$2', $path);

            self::$_modules[$moduleId] = new $className();
        }
    }
        
    protected static function _initializeAutoloader()
    {
        $includePaths = array();
        $includePaths[] = get_include_path();
        $includePaths[] = SITE_ROOT . DIRECTORY_SEPARATOR . 'code' . DIRECTORY_SEPARATOR . 'core';
        $includePaths[] = SITE_ROOT . DIRECTORY_SEPARATOR . 'code' . DIRECTORY_SEPARATOR . 'modules';
        $includePaths = implode(PATH_SEPARATOR, $includePaths);
        set_include_path($includePaths);

        spl_autoload_register('self::_autoload');
    }
    
    /**
     * @param string $class
     */
    protected static function _autoload($class)
    {
        self::log('Try to load : ' . $class, 'AUTOLOAD');
        require_once (str_replace('_', '/', $class) . '.php');
    }

    /**
     * @param string $string
     * @param string $type
     * @param string $file
     */
    public static function log($string, $type = 'LOG', $file = 'system')
    {
        $message = date('[Y-m-d H:i:s]') . " [".$type."] ". $string . PHP_EOL;
        file_put_contents(SITE_ROOT . "/var/log/" . $file, $message, FILE_APPEND | LOCK_EX);
    }
    
    /**
     * @param string $module
     *
     * @return \Aspid_Module|null
     */
    public static function getModule($module)
    {
        return isset(self::$_modules[$module]) ?
            self::$_modules[$module] :
                null;
    }
    
    /**
     * 
     * @return array $_modules
     */
    public static function getModules()
    {
        return self::$_modules;
    }
    
    /**
     * @param string $path
     * 
     * @return \Aspid_View|null
     */
    public static function getView($path)
    {
        list($module, $view) = explode('/', $path, 2);
        if ($module == 'Aspid')
        {
            $class = 'Aspid_View_' . $view;
            return new $class;
        }
        
        $module = self::getModule($module);
        if (!$module)
        {
            return null;
        }

        return $module->getView($view);
    }
    
    /**
     * @param string $path
     * 
     * @return \Aspid_Model|null
     */
    public static function getModel($path)
    {
        list($module, $model) = explode('/', $path, 2);

        $module = self::getModule($module);
        if (!$module)
        {
            return null;
        }
        return $module->getModel($model);
    }
    
    /**
     * @param string $path
     * @return \Aspid_Helper|null
     */
    public static function getHelper($path)
    {
        list($module, $model) = explode('/', $path, 2);

        $module = self::getModule($module);
        if (!$module)
        {
            return null;
        }

        return $module->getHelper($model);
    }
    
    /**
     * @return \Aspid_Db
     */
    public static function getDB()
    {
        return self::$_db;
    }
    
    /**
     * @return Aspid_Response
     */
    public static function getResponse()
    {
        if (empty(self::$_response))
        {
            self::$_response = new Aspid_Response;
        }
        return self::$_response;
    }

    /**
     * @return Aspid_Request
     */
    public static function getRequest()
    {
        if (empty(self::$_request))
        {
            self::$_request = new Aspid_Request();
        }
        return self::$_request;
    }

    /**
     * @return Aspid_Route
     */
    public static function getRoute()
    {
        if (empty(self::$_route))
        {
            self::$_route = new Aspid_Route();
        }
        return self::$_route;
    }
    
    public static function getConfig($key = null, $default = null)
    {
        if (null === $key)
        {
            return self::$_config;
        }

        return isset(self::$_config[$key]) ?
            self::$_config[$key] :
            $default;
    }
    
    public static function getBaseUrl()
    {
        return 'http://' . $_SERVER['HTTP_HOST'];
    }

    public static function getCurrentPath()
    {
        return implode('/', Aspid::getRequest()->getPath());
    }

    public static function getUrl($path)
    {
        $path = trim($path, '/');

        return self::getBaseUrl() . '/' . $path;
    }
    
    public static function runTest()
    {
//        self::_initializeAutoloader();
//        self::_initializeConfig();
//        self::_initializeDatabase();
//        self::_initializeModules();
//        $request = self::getRequest();
//        var_dump($request->getPath());
//        var_dump($request->getParameters());
//      $model = Aspid::getModel('Aspid_News/News');
//        
//        /*setting new model, saving it as table record printing ID*/
//        $model->setData('body', 'LOREM LOREM LOREM LOREM LOREM LOREM');
//        $model->setData('title', 'SECOND_TITLE');
//        $model->save();
//        /*LastInsert Id test*/
//        echo 'Last insert ID = ' . $model->getId();
//        
//        var_dump($model->getData());
//        
//        /*saving ID of model, we just saved in DB*/
//        $id = $model->getId();
//
//        /*loading not-existing empty model to clear data*/
//        $model->load('flush');
//        
//        /*loading model by saved ID and printing it`s data */
//        $model->load($id);
//        
//        /*updating some data in model and saving this update to DB */
//        $model->setData('body','UPDATED');
//        $model->save();
//        
//        /*rowCount test*/        
//        //echo $model->save();
//        
//        /*loading not-existing empty model to clear data*/
//        $model->load('flush');
//        
//        /*loading model by ID and printing it`s updated _data from DB*/
//        $model->load($id);
//        
//var_dump($model->getData());
//        
//        /*clearing models`s _data and deleting linked record in DB*/
//        $model->delete();
//        
//        /*loading not-existing empty model to clear data*/
//        $model->load('flush');
//            
//        /*making sure it`s gone*/
//        $model->load($id);
//        
//var_dump($model->getData());

        /*Test on false return in Aspid_Db->insert()*/
//        $model->setData('klbf', 'falseaustfd');
//        var_dump($model->save());
    }
}
<?php

class Aspid_Flat_Model_Reservation_Log extends Aspid_Model
{
    public function __construct()
    {
        $this->_map['table'] = 'flat_reservation_log';
        $this->_map['id'] = 'reservation_id';
        $this->_map['fields'] = [
            'date_time','status'
            ];
    }
}
<?php

class Aspid_Flat_Model_Reservation extends Aspid_Model
{

    protected $_flat;
    protected $_customer;
    protected $_log;

    public function __construct()
    {
        $this->_map['table'] = 'flat_reservation';
        $this->_map['id'] = 'id';
        $this->_map['fields'] = [
            'flat_id',
            'customer_id',
            'date_start',
            'date_end',
            'people',
            'message',
            'status'
        ];
    }

    public function getFlat()
    {
        if (null === $this->_flat)
        {
            $this->_flat = Aspid::getModel('Aspid_Flat/Flat');
            $this->_flat->load($this->getId());
        }

        return $this->_flat;
    }

    public function setFlat($flat)
    {
        $this->_flat = $flat;
    }

    public function getCustomer()
    {
        if (null === $this->_customer)
        {
            $this->_customer = Aspid::getModel('Aspid_Flat/Customer');
            $this->_customer->load($this->getId());
        }

        return $this->_customer;
    }

    public function setCustomer($customer)
    {
        $this->_flat = $flat;
    }

    public function getLog()
    {
        if (null === $this->_log)
        {
            $this->_images = Aspid::getModel('Aspid_Flat/Reservation_Log')
                    ->massLoad(array("reservation_id = " . $this->getId()));
        }

        return $this->_log;
    }

}

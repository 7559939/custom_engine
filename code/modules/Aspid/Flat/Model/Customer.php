<?php

class Aspid_Flat_Model_Customer extends Aspid_Model
{
    protected $_reservations;
    
    public function __construct()
    {
        $this->_map['table'] = 'flat_customer';
        $this->_map['id'] = 'id';
        $this->_map['fields'] = [
            'first_name',
            'last_name',
            'email',
            'phone',
            'messenger',
            'country',
            'language',
            'created',
            ];
    }
    
    /**
     * 
     * @return array of models Aspid_Flat_Model_Reservation
     */
    public function getReservations()
    {
        if (null === $this->_reservations)
        {
            $this->_reservations = Aspid::getModel('Aspid_Flat/Reservation')->massLoad(array("flat_id = " . $this->getId()));
        }
        
        return $this->_reservations;
    }
}
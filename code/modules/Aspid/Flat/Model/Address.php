<?php

class Aspid_Flat_Model_Address extends Aspid_Model
{
    protected $_flat;
    
    public function __construct()
    {
        $this->_map['table'] = 'flat_address';
        $this->_map['id'] = 'flat_id';
        $this->_map['fields'] = [
            'street',
            'building',
            'apartment', 
            'floor',
            'metro1',
            'metro2',
            'sqmeters',
            'elevator',
            'map_lat',
            'map_long',
        ];
    }

    public function getFlat()
    {
        if (null === $this->_flat)
        {   
            $this->_flat = Aspid::getModel('Aspid_Flat/Flat');
            $this->_flat->load($this->getId());
        }

        return $this->_flat;
    }
}

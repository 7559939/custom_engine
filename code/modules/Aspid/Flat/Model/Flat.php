<?php

class Aspid_Flat_Model_Flat extends Aspid_Model
{
    /**
     * Images.
     *
     * @var array
     */
    protected $_images;

    /**
     * Address.
     *
     * @var Aspid_Flat_Model_Address
     */
    protected $_address;

    /**
     * Reservations.
     *
     * @var array
     */
    protected $_reservations;

    public function __construct()
    {
        $this->_map['table'] = 'flat';
        $this->_map['id'] = 'id';
        $this->_map['fields'] = [
            'short_desc',
            'description',
            'rooms',
            'beds',
            'price',
        ];
    }

    /**
     * Returns array of Aspid_Flat_Model_Image objects.
     *
     * @return array
     */
    public function getImages()
    {
        if (null === $this->_images)
        {
            $this->_images = Aspid::getModel('Aspid_Flat/Image')->massLoad(array('flat_id = ' . $this->getId()));
        }

        return $this->_images;
    }

    /**
     * Can return only 1 model of type $type
     * 
     * @param $type
     * @return Aspid_Flat_Model_Image/null
     */
    public function getImageByType($type = 'main')
    {
        $images = $this->getImages();
        foreach ($images as $image)
        {
            if ($type == $image->getData('type'))
            {
                return $image;
            }
        }

        Aspid::log("image of '$type' type has not found in flat #" . $this->getData('id') . "  ", 'IMAGE');


        return $images ? reset($images) : null;
    }

    /**
     * 
     * @return Aspid_Flat_Model_Address
     */
    public function getAddress()
    {
        if (null === $this->_address)
        {
            $this->_address = Aspid::getModel('Aspid_Flat/Address');
            $this->_address->load($this->getId());
        }

        return $this->_address;
    }

    /**
     * 
     * @return array of models Aspid_Flat_Model_Reservation
     */
    public function getReservations()
    {
        if (null === $this->_reservations)
        {
            $this->_reservations = Aspid::getModel('Aspid_Flat/Reservation')->massLoad(array("flat_id = " . $this->getId()));
        }

        return $this->_reservations;
    }

}

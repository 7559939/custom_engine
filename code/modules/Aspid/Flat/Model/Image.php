<?php

class Aspid_Flat_Model_Image extends Aspid_Model
{
    public function __construct()
    {
        $this->_map['table'] = 'flat_images';
        $this->_map['id'] = 'flat_id';
        $this->_map['fields'] = [
            'filename',
            'type'
        ];
    }

    public function getUrl()
    {
        return Aspid::getBaseUrl() . '/images/flats/' . $this->getData('flat_id') .
                '/' . $this->getData('filename');
    }

    /**
     * 
     * @param int/Aspid_Flat_Model_Flat $flat
     * @return array of objects Aspid_Flat_Model_Image
     */
    public function massLoadByFlat($flat)
    {
        $class = 'Aspid_Flat_Model_Flat';
        return ($flat instanceof $class) ?
            $this->massLoad(array("flat_id =" . $flat->getId())) :
            $this->massLoad(array("flat_id =" . $flat));
    }

}

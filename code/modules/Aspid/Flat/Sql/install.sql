-- 1.1 Карточка квартиры
CREATE TABLE `flat` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`address` TEXT NULL,
	`short_desc` TEXT NULL,
	`description` TEXT NULL,
	`rooms` TINYINT(3) UNSIGNED NOT NULL,
	`beds` CHAR(30) NULL DEFAULT NULL,
	`price` SMALLINT(5) UNSIGNED NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5
;

-- 1.2 Карточка адреса квартиры
CREATE TABLE `flat_address` (
	`flat_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL,
	`street` CHAR(30) NOT NULL,
	`building` SMALLINT(6) NOT NULL,
	`apartment` SMALLINT(6) NOT NULL,
	`floor` TINYINT(4) NULL DEFAULT NULL,
	`metro1` CHAR(20) NULL DEFAULT NULL,
	`metro2` CHAR(20) NULL DEFAULT NULL,
	`sqmeters` CHAR(5) NULL DEFAULT NULL,
	`elevator` ENUM('true','false') NOT NULL DEFAULT 'false',
	`map_lat` DOUBLE NOT NULL DEFAULT '53.9045398',
	`map_long` DOUBLE NOT NULL DEFAULT '27.5615244'
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

-- 1.3 flat_customer
-- данные о клиенте, будет создаваться запись при создании брони, потом когда прикрутишь авторизацию - клиенту не надо будет заполнять кучу полей
CREATE TABLE `flat_customer` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` CHAR(50) NOT NULL,
	`first_name` CHAR(30) NULL DEFAULT NULL,
	`last_name` CHAR(30) NULL DEFAULT NULL,
	`email` VARCHAR(30) NOT NULL,
	`phone` VARCHAR(15) NULL DEFAULT NULL,
	`messenger` SET('viber','whatsUp','telegram') NULL DEFAULT NULL,
	`country` VARCHAR(30) NULL DEFAULT NULL,
	`language` VARCHAR(30) NULL DEFAULT NULL,
	`created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8
;


-- 1.4 Карточка фич квартиры
CREATE TABLE `flat_feature` (
	`flat_id` SMALLINT(5) UNSIGNED NOT NULL,
	`feature` CHAR(25) NOT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

-- 1.5 flat_images
CREATE TABLE `flat_images` (
	`flat_id` SMALLINT(5) UNSIGNED NOT NULL,
	`filename` CHAR(60) NOT NULL,
	`type` CHAR(15) NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

-- 1.6 Карточка бронирования
-- status - int (0 = pending, 1 = confirmed, 2 = cancelled, етц. моделька будет сама разбирать, что какая цифра значит)
CREATE TABLE `flat_reservation` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`flat_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL,
	`customer_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL,
	`date_start` DATE NULL DEFAULT NULL,
	`date_end` DATE NULL DEFAULT NULL,
	`people` TINYINT(4) NULL DEFAULT NULL,
	`message` VARCHAR(300) NULL DEFAULT NULL,
	`status` TINYINT(4) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5
;

-- 1.5. Лог бронирования
-- запоминаем, когда какая бронь сделана, и что с ней потом происходило. будет заполняться при создании брони, и при каждой смене статуса
CREATE TABLE `flat_reservation_log` (
	`reservation_id` SMALLINT(5) UNSIGNED NOT NULL,
	`date_time` DATETIME NULL DEFAULT NULL,
	`status` TINYINT(4) NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
<?php

class Aspid_Flat_Module extends Aspid_Module

{
    protected $_config = array(
        'enabled' => true,
        'name' => 'Aspid Flat',
        'description' => 'Allows posting and viewing Aspid flat.',
        'version' => '1.0.0',
        'frontName' => 'flat' 
    );
}
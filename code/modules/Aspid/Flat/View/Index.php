<?php

class Aspid_Flat_View_Index extends Aspid_View
{
    function __construct()
    {
        parent::__construct(SITE_ROOT . '/code/modules/Aspid/Flat/Design/templates/index.php');
    }

    protected function getFlatViewUrl($flatId) 
    {
        return Aspid::getUrl("flat/default/view/id/{$flatId}");
    } 
}

<?php $counter = 1; ?>
<?php foreach ($this->getData('flats') as $model): ?>
    <?php if ($counter == 1 || ($counter % 4 == 0)): ?>
        <div class="row">
    <?php endif; ?>
    <div class="col-sm-4 margin15">
        <div>
            <img class="img-responsive teaser-img" src="<?php echo $model->getImageByType()->getUrl() ?>">
            <div class="teaser-info">
                <div>
                    <h4>
                        <span class="label label-success margin0 pull-left">
                            <?php echo $model->getData('price')?>$
                        </span>
                        <span class="pull-right margin0">
                            <a href="<?php echo $this->getFlatViewUrl($model->getData('id')) ?>">
                                <?php echo $model->getAddress()->getData('street') . 
                                    ' ' . $model->getAddress()->getData('building') . ' ' ?>
                                <img src="<?php echo Aspid::getBaseUrl()?>/images/icons/street-map.png" />
                            </a>
                        </span>
                    </h4>
                </div>
                <div class="clearfix"></div>
                <div class="pull-left">
                    <h4>
                        <span class="label label-success">
                                <?php if ($model->getData('rooms') > 1): ?>
                                    <?php echo $model->getData('rooms') ?> rooms
                                <?php elseif ($model->getData('rooms') == 1): ?>
                                    <?php echo $model->getData('rooms') ?> room
                                <?php endif ?>
                        </span>
                    </h4>
                </div>
                <div class="pull-right"><?php echo $model->getAddress()->getData('metro1') . ' ' ?>
                    <img src="<?php echo Aspid::getBaseUrl()?>/images/icons/metro.png" />
                </div>
                <div class="clearfix"></div>

                <a class="btn btn-primary btn-sm pull-right"
                   href="<?php echo $this->getFlatViewUrl($model->getData('id')) ?>" role="button">
                    View more / Book
                </a>
                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="clearfix">
        </div>
    </div>

    <?php $counter++ ?>
    <?php if (($counter % 4) == 0): ?>
        </div>
    <?php endif; ?>
<?php endforeach; ?>

<!--Closing div with id = "content"-->
</div>
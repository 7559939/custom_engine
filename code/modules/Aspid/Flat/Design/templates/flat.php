<div class="flat-view">
    <div class="row">
        <div class="col-md-8">
            <div class="row carousel-holder">
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!--INDICATORS-->
                        <ol class="carousel-indicators">
                            <?php $flat = $this->getData('flat'); ?>
                            <?php $li = 0; ?>
                            <?php foreach ($flat->getImages() as $image): ?>
                                <li data-target="#myCarousel" 
                                    data-slide-to="<?php echo $li ?>" 
                                    <?php if ($li == 0): ?> 
                                        class='active'
                                    <?php endif ?>/>
                                </li>
                                <?php $li++; ?>
                            <?php endforeach ?>
                        </ol>

                        <!--SLIDES-->
                        <div class="carousel-inner">

                            <?php $slide = 0; ?>
                            <?php foreach ($flat->getImages() as $image): ?>
                                
                                    <div class="item<?php if ($slide == 0): ?>
                                         active<?php endif ?>">
                                        <img class="slide-image slide750x500" 
                                             src="<?php echo $image->getUrl() ?>" 
                                             alt="Minsk apartment for rent, ">		   
                                    </div>
                                <?php $slide++; ?>
                            <?php endforeach ?>
                        </div>

                        <a class="left carousel-control" 
                           href="#myCarousel" data-slide="prev">
                           <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" 
                            href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>

                    </div>
                </div>
            </div>

            <!-- Map -->
            <hr>
            <div class="col-md-12" id="googleMap" style="width:100%; height:400px; padding:0px">
<!--                --><?php //var_dump($flat->getAddress())?>
            </div>
        </div>

        <div class="col-md-4" id="flat-desc">
            <div class="thumbnail">
                <div class="caption-full">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-xs-6 col-md-12">Price: <?php echo $flat->getData('price') ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-12">Guests: <?php echo $flat->getData('beds') ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-12">Rooms: <?php echo $flat->getData('rooms') ?></div>
                            </div>
                        </div>
                    </div>
                    <h3 class="header">Description</h3>
                    <?php echo $flat->getData('description') ?>
                </div>
            </div>
        </div>
        <div class="col-md-4" id="flat-desc">
            <div class="thumbnail">
                <h3 class='caption-header'>Booking details</h3>
                <form id="bookingform" name="bookingform" method="post">
                    <!--action="<?php // echo Aspid::getBaseUrl()?>/flat/booking"-->
                    
                    Check-in date:* <input type="date" name="date_start" class="pull-right">
                    <hr>
                    Check-out date:* <input type="date" name="date_end" class="pull-right">
                    <hr>
                    People:* <input type="text" name="people" placeholder="2 couples" 
                        class="pull-right">
                    <hr>
                    Your name:* <input type="text" name="name" placeholder="James Jameson" class="pull-right">
                    <hr>
                    Email:* <input type="email" name="email" placeholder="Jameson@gmail.com" class="pull-right">
                    <hr>
                    Phone: <input type="text" name="phone" class="pull-right">
                    <br><div class="clearfix"></div>
                    <input type="checkbox" name="Viber" value="Viber"> Viber
                    <input type="checkbox" name="whatsUp" value="whatsUp"> whatsUp
                    <input type="checkbox" name="telegram" value="telegram"> telegram
                    <hr>
                    Message:*<br>
                    <textarea type="text" name="message" rows="15" cols="38" ></textarea>
                    <br><br>
                    <button type="submit" class="btn btn-primary" onclick="bookingValidation()">Send booking request</button>
<!--                    <br><br>
                    <button type="submit" class="btn btn-primary">Add to multiple booking</button>-->
                </form>
                
            </div>
        </div>
    </div>
</div>

<script src="http://maps.googleapis.com/maps/api/js"></script>

<script>
    var myCenter = new google.maps.LatLng(
        <?php echo $flat->getAddress()->getData('map_lat') ?>,
        <?php echo $flat->getAddress()->getData('map_long') ?>
    );

    function initialize()
    {
        var mapProp = {
            center: myCenter,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        var marker = new google.maps.Marker({
            position: myCenter,
        });

        marker.setMap(map);
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php if ($requestPost = Aspid::getRequest()->getPost()): ?>
    <?php
    $customer = Aspid::getModel('Aspid_Flat/Customer');
    $customer->setData('name', $requestPost->getPost('name'))
            ->setData('email', $requestPost->getPost('email'))
            ->setData('phone', $requestPost->getPost('phone'))
            ->save()
    ?>
    <?php $customer_id = $customer->getId() ?>
    <?php
    $reservation = Aspid::getModel('Aspid_Flat/Reservation')
            ->setData('flat_id', $flat->getId())
            ->setData('customer_id', $customer_id)
            ->setData('date_start', $requestPost->getPost('date_start'))
            ->setData('date_end', $requestPost->getPost('date_end'))
            ->setData('people', $requestPost->getPost('people'))
            ->setData('message', $requestPost->getPost('message'))
            ->save();
    ?>
<?php endif?>
<?php if (Aspid::getRequest()->getGet('pass')== 'add'): ?>

    <h1>Add new apartment</h1>
    <form enctype="multipart/form-data" method="post" action="<?php echo Aspid::getBaseUrl() . '/flat/add/addHandler' ?>">
        <div class="col-md-4">
            Short Description: <br><input type="text" name="short_desc"><div class="clearfix"></div>
            Description: <br><textarea type="text" name="description" rows="15" cols="38" ></textarea><div class="clearfix"></div>
            Rooms: <br><input type="text" name="rooms" required><div class="clearfix"></div>
            Beds: <br><input type="text" name="beds" ><div class="clearfix"></div>
            Price: <br><input type="text" name="price" required><div class="clearfix"></div>
        </div>
        <div class="col-md-4">
            Street: <br><input type="text" name="street" required><div class="clearfix"></div>
            Building: <br><input type="text" name="building" required><div class="clearfix"></div>
            Apartment: <br><input type="text" name="apartment"><div class="clearfix"></div>
            Floor: <br><input type="text" name="floor"><div class="clearfix"></div>
            elevator : <input type="checkbox" name="elevator"><div class="clearfix"></div>
            Metro1: <br><input type="text" name="metro1"><div class="clearfix"></div>
            Metro2: <br><input type="text" name="metro2"><div class="clearfix"></div>
            square_meters: <br><input type="text" name="sqmeters"><div class="clearfix"></div>
            map_lat: <br><input type="text" name="map_lat"><div class="clearfix"></div>
            map_long: <br><input type="text" name="map_long"><div class="clearfix"></div>
        </div>
        <div class="col-md-4">
            <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
            <p>Choose <b>MAIN</b> image FILE</p>
            <input type="file" name="images[]" />
            <hr>
            <p>Choose <b>OTHER</b> image FILES</p>
            <input type="file" multiple name="images[]" />
            <hr>
            <input type="submit" value="Send FILES and save DATA" />
        </div>
    </form>

<?php else : echo 'Go away' ?>

<?php endif ?>
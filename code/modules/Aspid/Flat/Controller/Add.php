<?php

class Aspid_Flat_Controller_Add extends Aspid_Controller
{
    public function defaultAction()
    {
        return $this->addAction();
    }
    
    public function notfoundAction()
    {
        return $this->defaultAction();
    }
    
    public function addAction()
    {
        $view = Aspid::getView('Aspid_Flat/Add');
        return $view;
    }
    
    public function addHandlerAction()
    {
//      Saving data
        $requestPost = Aspid::getRequest()->getPost(); 
        
//      Table flat
        
            $flat = Aspid::getModel('Aspid_Flat/Flat');
            $flat->setData('short_desc', $requestPost['short_desc'])
                ->setData('description', $requestPost['description'])
                ->setData('rooms', $requestPost['rooms'])
                ->setData('beds', $requestPost['beds'])
                ->setData('price', $requestPost['price'])
                ->save();

            $flat_id = $flat->getId();
            

//      Table flat_address
            
            $elevator = (isset($requestPost['elevator'])) ? 1 : 0;
            $address = Aspid::getModel('Aspid_Flat/Address');
            $address->setData('flat_id', $flat->getId())
                    ->setData('street', $requestPost['street'])
                    ->setData('building', $requestPost['building'])
                    ->setData('apartment', $requestPost['apartment'])
                    ->setData('floor', $requestPost['floor'])
                    ->setData('elevator', $elevator)
                    ->setData('metro1', $requestPost['metro1'])
                    ->setData('metro2', $requestPost['metro2'])
                    ->setData('sqmeters', $requestPost['sqmeters'])
                    ->setData('map_lat', $requestPost['map_lat'])
                    ->setData('map_long', $requestPost['map_long'])
                    ->saveWOAI();
            
//    Saving files
//    Table flat_images
            
        if ($_FILES && $_FILES['images'])
        {
            if (($_FILES['images']['error'][0] != 4) && ($_FILES['images']['error'][1] != 4))
            {
                $ds = DIRECTORY_SEPARATOR;
                $dir = SITE_ROOT . $ds . 'images' . $ds . 'flats' . $ds .  $flat_id;
                mkdir($dir);

                foreach ($_FILES['images']['error'] as $key => $error)
                {
                    $imageType = '';

                    if (0 == $key)
                    {
                        $imageType = 'main';
                    }

                    if ($error == UPLOAD_ERR_OK)
                    {
                        $tmpName = $_FILES['images']['tmp_name'][$key];
                        $name = $_FILES['images']['name'][$key];

                        $image = Aspid::getModel('Aspid_Flat/Image');
                        $image->setData('flat_id', $flat_id)
                              ->setData('type', $imageType)
                              ->setData('filename', $name)
                              ->saveWOAI();

                        move_uploaded_file($tmpName, $dir . $ds . $name);
                        
                    }
                }
            }
        }
        
        header('Location: '. Aspid::getBaseUrl() . '/flat/add/add?pass=add');
    }
}
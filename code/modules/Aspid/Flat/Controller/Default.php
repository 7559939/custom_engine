<?php

class Aspid_Flat_Controller_Default extends Aspid_Controller
{
    public function defaultAction()
    {
        return $this->showFlatsAction();
    }
    
    public function notfoundAction()
    {
        return $this->defaultAction();
    }
    
    public function viewAction()
    {
//        Setting JS to html <head>, stored in Aspid_Response->_body
        Aspid::getResponse()->getBody()->getChild('head')
            ->addJs('js/jquery.validate.min.js')
            ->addJs('js/bookingValidation.js');
        
        $view = Aspid::getView('Aspid_Flat/Flat');
        $model = Aspid::getModel('Aspid_Flat/Flat');
        
        $id = Aspid::getRequest()->getParameters('id');
        $model->load($id);
        $view->setData('flat', $model);
        return $view;
    }
    
    public function showFlatsAction()
    {
        $view = Aspid::getView('Aspid_Flat/Index');
        $model = Aspid::getModel('Aspid_Flat/Flat');
        $view->setData('flats', $model->massLoad());
        return $view;
    }
    
    public function oneroomAction()
    {
        $view = Aspid::getView('Aspid_Flat/Index');
        $model = Aspid::getModel('Aspid_Flat/Flat');
        $view->setData('flats', $model->massLoad(array("rooms = 1")));
        return $view;
    }
    
    public function tworoomAction()
    {
        $view = Aspid::getView('Aspid_Flat/Index');
        $model = Aspid::getModel('Aspid_Flat/Flat');
        $view->setData('flats', $model->massLoad(array("rooms = 2")));
        return $view;
    }
    
    public function threeroomAction()
    {
        $view = Aspid::getView('Aspid_Flat/Index');
        $model = Aspid::getModel('Aspid_Flat/Flat');
        $view->setData('flats', $model->massLoad(array("rooms = 3")));
        return $view;
    }
    
    
    public function testAction()
    {
        $view = Aspid::getView('Aspid_Flat/Index');
        $flat = Aspid::getModel('Aspid_Flat/Flat');
        for($i=5;$i++;$i<100)
        {
            $flat->delete($i);
        }

        return $view;
    }
}
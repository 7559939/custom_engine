<?php

class Aspid_Flat_Controller_Booking extends Aspid_Controller
{
    public function defaultAction()
    {
        return $this->readFlatAction();
    }
    
    public function notfoundAction()
    {
        return $this->defaultAction();
    }
    
    public function readFlatAction()
    {
        $view = Aspid::getView('Aspid_Flat/Flat');
        $model = Aspid::getModel('Aspid_Flat/Flat');
            
        $view->setData('flat', $model->load());
        return $view;
    }
}
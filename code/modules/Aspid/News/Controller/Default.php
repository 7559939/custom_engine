<?php

class Aspid_News_Controller_Default extends Aspid_Controller
{
    public function defaultAction()
    {
        return $this->readTableNewsAction();
    }
    
    public function notfoundAction()
    {
        return $this->defaultAction();
    }
    
    public function readTableNewsAction()
    {
        $view = Aspid::getView('Aspid_News/News');
        $model = Aspid::getModel('Aspid_News/News');
        $view->setData('news', $model->massLoad());
        return $view;
    }
    
    public function randomizeAction()
    {
        $model = Aspid::getModel('Aspid_News/News');
        $model->massDelete();
        $helper = Aspid::getHelper('Aspid_News/News');
        for ($i = 0; $i < 9; $i++)
        {
            $model = Aspid::getModel('Aspid_News/News');
            $model->setData('title', $helper->gennadI())
                    ->setData('body', $helper->gennadII())
                    ->save();
        }
        return $this->defaultAction();
    }
}
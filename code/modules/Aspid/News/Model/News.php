<?php
/**
 * Description of News
 *
 * @author _@sp1D_
 */
class Aspid_News_Model_News extends Aspid_Model
{
    public function __construct()
    {
        $this->_map['table'] = 'news';
        $this->_map['id'] = 'id';
        $this->_map['fields'] = ['title','body','date'];
    }
}
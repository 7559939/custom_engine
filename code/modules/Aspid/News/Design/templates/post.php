<div class="col-sm-4 margin15">
    <div>
        <img class="img-responsive teaser-img" src="http://placehold.it/340x180">

        <div class="teaser-info">

            <div>
                <p>
                    <span class="label label-success margin0 pull-left"><?php echo $this->_data['post']['id'] ?>$</span>
                    <span class="pull-right margin0"><?php echo $this->_data['post']['title'] ?></span> 
                </p>
            </div>

            <div class="clearfix"></div>

            <div class="pull-left">
                <span class="label label-success">
                    X rooms
                </span>
            </div>
            <div class="pull-right">Metro</div>
            <div class="clearfix"></div>

            <a class="btn btn-primary btn-sm pull-right" href="#" role="button">
                View more / Book</a>
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
</div>
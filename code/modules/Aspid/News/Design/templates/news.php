<?php foreach ($this->getData('news') as $model): ?>
    
    <?php if ($this->getCounter() == 0): ?>
        <div class="row">
    <?php endif; ?>

    <?php $data = $model->getData();
    $this->setCounter($this->getCounter() + 1);
    $this->renderPost($data); ?>

    <?php if ($this->getCounter() == 3): ?>
        </div>
        <div class="clearfix"></div>
        <?php $this->setCounter(0); ?>
    <?php endif; ?>
        
<?php endforeach; ?>
        
<?php if ($this->getCounter() != 0) : ?>
        </div>
        <div class="clearfix"></div>
<?php endif;
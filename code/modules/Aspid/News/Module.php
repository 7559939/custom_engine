<?php

class Aspid_News_Module extends Aspid_Module

{
    protected $_config = array(
        'enabled' => true, // значит модуль включен
        'name' => 'Aspid News', // имя модуля, чисто для красоты отображения в админке
        'description' => 'Allows posting and viewing Aspid news.', // краткое описание модуля для админки
        'version' => '1.0.0', // версия модуля
        'frontName' => 'news' // для роутинга - означает что урлы вида http://aspid.com/news/*** пойдут на этот модуль
    );
}
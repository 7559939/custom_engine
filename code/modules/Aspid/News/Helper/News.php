<?php

class Aspid_News_Helper_News
{
    private $_lowerCase = 'abcdefghijklmnopqrstuvwxyz';
    private $_upperCase = 'ABCDEFGHIJKLMNOPRSTUVWXYZ';
    private $_signs = '.!?';
    
    public function gennadI()
    {
      return $this->sentGena(2, 2);
    }
    
    public function gennadII()
    {
        $body = '';
        
        for ($i=0; $i < mt_rand(2, 3) - 1; $i++)
        {
            $body .= $this->sentGena(2, 4);
            $body .= '<br>';
        }

        return $body;
    }
    
    public function wordGena($charMin, $charMax, $isUpper = null)
    {
        $charNumber = mt_rand($charMin, $charMax);
        $word = '';
        if ($isUpper)
        {
            $word = $this->_upperCase[mt_rand(0, strlen($this->_upperCase) - 1)];
            $charNumber = $charNumber - 1;
        }
        
        for ($i = 0; $i < $charNumber; $i++)
        {
            $word .= $this->_lowerCase[mt_rand(0, strlen($this->_lowerCase) - 1)];
        }
        
        return $word;
    }
    
    public function sentGena($wordMin, $wordMax)
    {
        $wordNumber = mt_rand($wordMin, $wordMax);
        $sentense = $this->wordGena(3,12,'isUP');
        for ($i = 0; $i < ($wordNumber-1); $i++)
        {
            $sentense .= ' ' . $this->wordGena(2,9);
        }
        $sentense = $this->commaGena($sentense);
        $sentense .= $this->signGena();
        return $sentense;
    }
    
    public function signGena()
    {
        return $this->_signs[mt_rand(0, 2)];
    }
    
    public function commaGena($sentense)
    {
        $arr = explode(' ', $sentense);
        $return = '';

        for ($i = 0; $i < count($arr)-1; $i++)
        {
            if (mt_rand(0,4) == 0)
            {
                $return .= $arr[$i] . ', ' ;
            }
            else
            {
                $return .= $arr[$i] . ' ';
            }
            
            $return .= $arr[$i];
        }
    return $return;
    }
}

<?php

class Aspid_News_View_News extends Aspid_View
{    
    private $teaserCounter = 0;
    
    function __construct()
    {
        parent::__construct(SITE_ROOT . '/code/modules/Aspid/News/Design/templates/news.php');
    }
    
    function renderPost($data)
    {
        $post = Aspid::getView('Aspid_News/Post');
        $post->setData('post', $data);
        echo $post->render();
    }
        
    public function getCounter()
    {
        return $this->teaserCounter;
    }
    
    public function setCounter($value)
    {
        $this->teaserCounter = $value;
    }
}
<?php

abstract class Aspid_Module
{
    /**
     * Module configuration.
     *
     * @var array
     */
    protected $_config = array();

    /**
     * Base class for all module elements.
     *
     * @var string
     */
    protected $_baseClass = null;

    /**
     * Returns module configuration.
     *
     * If $key is specified, returns keyed value, if it's not found returns $default.
     *
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function getConfig($key = null, $default = null)
    {
        if (null === $key)
        {
            return $this->_config;
        }

        return isset($this->_config[$key]) ?
            $this->_config[$key] :
            $default;
    }

    /**
     * Returns base class name.
     *
     * @return string
     */
    public function getBaseClass()
    {
        if (null === $this->_baseClass)
        {
            $class = get_class($this);
            $this->_baseClass = str_replace('_Module', '', $class);
        }

        return $this->_baseClass;
    }

    /**
     * Returns an instance of a view.
     *
     * @return Aspid_View
     */
    public function getView($view)
    {
        $view = explode('/', $view);
        $view = array_map('ucfirst', $view);
        $view = implode('_', $view);

        $class = $this->getBaseClass() . '_View_' . $view;

        return new $class();
    }
    
    /**
     * Returns an instance of a model.
     *
     * @return Aspid_Model
     */
    public function getModel($model)
    {
        $model = explode('/', $model);
        $model = array_map('ucfirst', $model);
        $model = implode('_', $model);

        $class = $this->getBaseClass() . '_Model_' . $model;

        return new $class();
    }

    public function getControllerClass($className)
    {
        return $this->getBaseClass() . '_Controller_' . ucfirst($className);
    }
            
    public function getController($urlParsedControllerName)
    {   
        $classPath = str_replace('_', DIRECTORY_SEPARATOR ,$this->getControllerClass($urlParsedControllerName));
        $path = SITE_ROOT . DIRECTORY_SEPARATOR . 'code' . DIRECTORY_SEPARATOR . 
                'modules' . DIRECTORY_SEPARATOR . $classPath . '.php';
        
        if ($urlParsedControllerName == 'default' || !file_exists($path))
        {
            $controller = $this->getControllerClass($this->getConfig('defaultControllerFront', 'default'));
            Aspid::log('default. Given default or controler that does not exist', 'CONTROLLER');
        }
        else
        {
            $controller = $this->getControllerClass($urlParsedControllerName);
        }
        return new $controller;
    }
    
    /**
     * Returns an instance of a helper.
     *
     * @return Aspid_Helper
     */
    public function getHelper($helper)
    {
        $helper = explode('/', $helper);
        $helper = array_map('ucfirst', $helper);
        $helper = implode('_', $helper);

        $class = $this->getBaseClass() . '_Helper_' . $helper;

        return new $class();
    }
    
}
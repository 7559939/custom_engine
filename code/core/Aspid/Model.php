<?php

abstract class Aspid_Model
{

    /**
     * @var array $_map
     * 
     * @var string $_map['table'] - table`s name in database
     * @var string $_map['id'] - column`s name of unique identificator (usually autoincrement, 'id')
     * @var array $_map['fields'] array of table`s fields(colunms) (w/o key column(aka 'id' column))  
     */
    protected $_map;

    /**
     * @var array $_data
     */
    protected $_data;

    /**
     * Load data from database to Model`s $_data
     * 
     * @param mixed $id
     */
    public function load($id)
    {
        $condition = array($this->getIdentifier() . ' = ' . $id);
        $pdoStatement = Aspid::getDB()->select($this->getTable(), $this->getAllFields(), $condition);

        $this->_data = $pdoStatement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Write/update database record
     * 
     * To write a record in database from model,
     * do not set model`s id-field, it will be set automatically,
     * with a key returned created database record.
     *
     * If object`s ID-field in $_data is set - record in database
     * will be updated. 
     */
    public function save()
    {
        if ($this->getId())
        {
            foreach ($this->getData() as $key => $value)
            {
                $sqlParameter[] = $key . ' = \'' . $value . '\'';
            }
            $condition[] = $this->getIdentifier() . ' = ' . $this->getId();

            Aspid::getDB()->update($this->getTable(), $sqlParameter, $condition);
        } else
        {
            foreach ($this->getData() as $key => $value)
            {
                $cols[] = $key;
                $values[] = "'$value'";
            }
            $this->setData($this->getIdentifier(), Aspid::getDB()->insert($this->getTable(), $cols, $values));
        }
    }

    /**
     * Strict insert of record into database
     * 
     * For a records without AUTO_INCREMENT key field
     *
     */
    public function saveWOAI()
    {
        foreach ($this->getData() as $key => $value)
        {
            $cols[] = $key;
            $values[] = "'$value'";
        }
        $this->setData($this->getIdentifier(), Aspid::getDB()->insert($this->getTable(), $cols, $values));
    }

    /**
     * Delete record from database 
     */
    public function delete()
    {
        if ($this->getId())
        {
            Aspid::getDB()->delete($this->getTable(), array($this->getIdentifier() . ' = ' . $this->getId()));
            $this->setData();
            Aspid::log('Model ' . get_class($this) . ', id = ' . $this->getId() . ' has been deleted', 'MODEL');
        } else
        {
            Aspid::log('Can not delete record that does not exist in DataBase', 'MODEL');
        }
    }

    /**
     * 
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setData($key = null, $value = null)
    {

        if (isset($key) && isset($value))
        {
            $this->_data[$key] = $value;
        } elseif (isset($key))
        {
            $this->_data = $key;
        } else
        {
            $this->_data = null;
        }
        return $this;
    }

    /**
     * @param mixed $key
     * @param mixed $default
     * @return type mixed
     */
    public function getData($key = null, $default = null)
    {
        if (isset($key))
        {
            return isset($this->_data[$key]) ?
                    $this->_data[$key] :
                    $default;
        }
        return isset($this->_data) ?
                $this->_data :
                $default;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getData($this->getIdentifier());
    }

    /**
     * @return $this
     */
    public function getInstance()
    {
        $class = get_class($this);
        return new $class;
    }

    /**
     * @param array $conditions
     * @param array $orderBy
     * @param array $limit
     *
     * @return array    Array of objects Aspid_Model
     */
    public function massLoad(array $conditions = array(), array $orderBy = array(), array $limit = array())
    {
        $table = $this->getTable();
        $fields = $this->getAllFields();
        $massModels = array();
        $pdoStatement = Aspid::getDB()->select($table, $fields, $conditions, $orderBy, $limit);

        if (false === $pdoStatement)
        {
            Aspid::log('ERROR IN MASSLOAD: ' . Aspid::getDB()->getLastError(), "MODEL");
            return array();
        }

        foreach ($pdoStatement->fetchAll(PDO::FETCH_ASSOC) as $index => $recordArray)
        {
            $model = $this->getInstance();
            $model->setData($recordArray);
            $massModels[] = $model;
        }

        if (!$massModels)
        {
            Aspid::log("Empty array returned on massLoad(). ErrorInfo(): " .
                Aspid::getDB()->getLastError() . ' ' .
                $pdoStatement->queryString, "MODEL"
            );
        }

        return $massModels;
    }

    /**
     * Delete 
     * 1)All records if empty($conditions). 
     * 2)OR records under the terms of $conditions if !empty($conditions). 
     * From $this->_map['table'] in database
     */
    public function massDelete(array $conditions = array())
    {
        return Aspid::getDB()->delete($this->getTable(), $conditions);
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->_map['table'];
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->_map['id'];
    }

    /**
     * @return string
     */
    public function getFields()
    {
        return $this->_map['fields'];
    }

    /**
     * @return string
     */
    public function getAllFields()
    {
        return array_merge(array($this->_map['id']), $this->_map['fields']);
    }

    /**
     *  Returns number of selected rows
     * 
     * @return int rowsAffected
     */
    public function getSelectedRows(array $conditions = array(), array $orderBy = array(), array $limit = array())
    {
        $table = $this->getTable();
        $number = Aspid::getDB()->select($table, array("COUNT(*)"), $conditions, $orderBy, $limit);
        return $number->fetchColumn();
    }

}

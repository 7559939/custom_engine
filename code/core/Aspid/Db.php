<?php

class Aspid_Db
{
    /**
     * Configuration.
     *
     * @var array
     */
    protected $_config;

    /**
     * Database connection object.
     *
     * @var PDO
     */
    protected $_connection;

    /**
     * Aspid_Db constructor.
     *
     * @param array $config Array of DB connection requisites:
     *      'dbname' => 'value',
     *      'host' => 'value',
     *      'username' => 'value',
     *      'password' => 'value'
     */
    public function __construct($config)
    {
        $this->_config = $config;
    }
    
    /**
     * Establishing connection
     */
    protected function _connect()
    {
        $dsn = "mysql:dbname={$this->_config['dbname']};host={$this->_config['host']}";

        $this->_connection = new PDO(
            $dsn,
            $this->_config['username'],
            $this->_config['password']
        );
    }

    /**
     * @return PDO
     */
    protected function _getConnection()
    {
        if (null === $this->_connection)
        {
            $this->_connect();
        }

        return $this->_connection;
    }
 
    /**
     * Executes SQL query.
     *
     * @param string $query
     *
     * @return PDOStatement
     */
    public function query($query)
    {
        Aspid::log($query, 'DB', 'DB');
        return $this->_getConnection()->query($query);
    }

    /**
     * Returns data extracted from database table using input query parameters.
     *
     * @param string $table
     * @param array $fields
     * @param array $conditions
     * @param array $order
     * @param int $limit
     *
     * @return PDOStatement
     */
    public function select($table, array $fields = array('*'), array $conditions = array(), array $order = array(), $limit = array())
    {
        $fields = implode(', ', $fields);
        if (!isset($fields))
        {
            $fields = '*';
        }
        $query_text = "SELECT $fields FROM $table";

        if (!empty($conditions))
        {
            $conditions = implode(' AND ', $conditions);
            $query_text .= " WHERE $conditions";
        }

        if (!empty($order))
        {
            $order = implode(', ', $order);
            $query_text .= " ORDER BY $order";
        }

        if (!empty($limit)) 
        {
            $limit = implode(', ', $limit);
            $query_text .= " LIMIT $limit";
        }
        
        return $this->query($query_text);
    }

    /**
     * Inserting data into database
     * @return int|false
     */
    public function insert($table, array $fields = array(), array $values = array())
    {
        $fields = implode(', ', $fields);
        $values = implode(', ', $values);
        
        $insertReturn = $this->query("INSERT INTO $table ($fields) VALUES ($values)");
        
        return ($insertReturn) ?
            $this->getLastInsertedId() :
                $insertReturn;
    }

    /**
     * Updating record in database
     * 
     * @param array $data (column1=value1,column2=value2,...)
     * @param array $conditions (some_column=some_value)
     * @return int rowCount()
     */
    public function update($table, array $data = array(), array $conditions = array())
    {

        $data = implode(', ', $data);
        $conditions = implode(' ', $conditions);
        $return = $this->query("UPDATE {$table} SET {$data} WHERE {$conditions}");
        
        return $return->rowCount();
    }

    /**
     * Delete record from database
     * @return int rowCount()
     */
    public function delete($table, array $conditions)
    {   
        if(!empty($conditions))
        {
            $conditions = implode(' ', $conditions);
            return $this->query("DELETE FROM {$table} WHERE {$conditions}")->rowCount();
        }
        else
        {
            return $this->query("DELETE FROM {$table}")->rowCount();
        }
    }
    
    /**
     * Returns id of last record, inserted in database
     * 
     * @return mixed
     */
    public function getLastInsertedId()
    {
        return $this->_getConnection()->lastInsertId();
    }
    
    /**
     * Returns last error
     * 
     * @return mixed
     */
    public function getLastError()
    {
        return implode('\n', $this->_getConnection()->errorInfo());
    }
}
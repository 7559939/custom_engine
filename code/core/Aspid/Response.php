<?php

class Aspid_Response
{

    /**
     * @var array 
     */
    protected $_headers = array();

    /**
     * @var array 
     */
    protected $_body = array();

    /**
     * @return string body
     */
    public function send()
    {
        foreach ($this->getHeaders() as $header)
        {
            header($header);
        }
        echo $this->prepareBody($this->getBody());
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->_headers = $headers;
        return $this;
    }

    /**
     * @param mixed $header
     * @return \Aspid_Response
     */
    public function addHeader($header)
    {
        $this->_headers[] = $header;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @param mixed $body
     * @return \Aspid_Response
     */
    public function setBody($body)
    {
        $this->_body = $body;
        return $this;
    }

    /**
     * @param mixed $body
     * @return array $_headers
     */
    public function addContent($content)
    {
        $this->_body[] = $content;
        return $this;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @param mixed (Aspid_View object more often)
     * @return string
     */
    public function prepareBody($body)
    {
        $output = '';

        if (is_array($body))
        {
            foreach ($body as $element)
            {
                $output .= $element;
            }
        } else
        {
            $output .= $body;
        }

        return $output;
    }

}

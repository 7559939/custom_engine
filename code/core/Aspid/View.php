<?php

class Aspid_View
{
    /**
     * @var string 
     * свойство для хранения пути к темплейту
     */
    
    protected $_template; 
    
    /**
     * @var array 
     * массив для разных переменных которые будут использоваться в темплейте
     */
    protected $_data; 
    
    /**
     * @var array 
     * массив для вложенных вьюшек
     */
    protected $_children; 
    
    /**
     * @param string $template
     * @param array $data
     * @param array $children
     */
    public function __construct($template = null, array $data = array(), array $children = array())
    {
        $this->_template = $template;
        $this->_data = $data;
        $this->_children = $children;
    }

    /**
     * @param string $template
     * @return \Aspid_View
     */    
    public function setTemplate($template)
    {
        $this->_template = $template;
        return $this;
    }
    
    /**
     * @return \Aspid_View or null
     */
    public function getTemplate()
    {        
        if (!empty($this->_template))
        {
            return $this->_template;
        }
        else            
        {
            Aspid::log($this . '->_template is empty', 'VIEW');
            return null;            
        }
    }
    
    /**
     * @param mixed $key
     * @param mixed $value
     * @return \Aspid_View
     */
    public function setData($key, $value)
    {
        $this->_data[$key] = $value;
        return $this;
    }
    
    /**
     * @param string $dataKey
     * @return mixed
     */
    public function getData($dataKey)
    {
        if (isset($this->_data[$dataKey]))
        {
            return $this->_data[$dataKey];
        }
        else
        {
            Aspid::log('getData('. $dataKey . ') is empty', 'VIEW');
            return null;
        }
    }
    
    /**
     * @return \Aspid_View
     */
    public function getChildren()
    {
        if (!empty($this->_children))
        {
            return $this->_children;
        }
        else
        {
            Aspid::log(__CLASS__ . '->_children is not found', 'VIEW');
            return null;
        }
    }
    
    /**
     * @param mixed $giveMeChild
     * @return \Aspid_View
     */
    public function getChild($giveMeChild)
    {
        if (isset($this->_children[$giveMeChild]))
        {
            return $this->_children[$giveMeChild];
        }
        else
        {
            Aspid::log(__CLASS__ . '->_child[' . $giveMeChild . '] is not found', 'VIEW');
            return null;
        }
    }
    
    /**
     * @param mixed $childKey
     * @param mixed $childValue
     * @return \Aspid_View
     */
    public function addChild($childKey, $childObject)
    {
        $this->_children[$childKey] = $childObject;
        return $this;
    }

    /**
     * @param mixed $childKey
     * @return \Aspid_View
     */
    public function removeChild($childKey)
    {
        unset($this->_children[$childKey]);
        return $this;
    }

    /**
     * @return string View as Html-string
     */
    public function render()
    {
        if (!file_exists($this->_template))
        {
            Aspid::log('template file not found: ' . $this->_template, 'TPL');
            return '';
        }

        ob_start();
        include $this->_template;
        $html = ob_get_clean();

        if (Aspid::getConfig('DEBUG'))
        {
            $html = '<div style="border: 1px dotted #f00; margin: 2px;">' .
                '<div style="background-color: #f00; color: #fff;">' . $this->_template . '</div>' .
                '<div style="background-color: #f00; color: #fff;">' . get_class($this) . '</div>' .
                $html .
                '</div>';
        }

        return $html;
    }
    
    /**
     * @param $child
     *
     * @return string
     */
    public function renderChild($child)
    {        
        return $this->getChild($child)
            ->render();
    }
    
    /**
     * @return string Rendered View
     */
    public function __toString()
    {
        return $this->render();
    }
    
    public function getBaseUrl()
    {
        return 'http://' . $_SERVER['SERVER_NAME'];
    }
}
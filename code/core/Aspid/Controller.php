<?php

class Aspid_Controller
{
        
    public function dispatch()
    {
        $this->_preDispatch();
        $this->_dispatch();
        $this->_postDispatch();
    }
    
    protected function _preDispatch()
    {   
        $page = Aspid::getView('Aspid/Page');
        $body = Aspid::getView('Aspid/Page_Body');
        $body->addChild('header', Aspid::getView('Aspid/Page_Body_Header'))
             ->addChild('footer', Aspid::getView('Aspid/Page_Body_Footer'))
             ->addChild('menu', Aspid::getView('Aspid/Page_Body_Menu'))
             ->addChild('sidebar', Aspid::getView('Aspid/Page_Body_Sidebar'));

        $head = Aspid::getView('Aspid/Page_Head');
        $head->addCss('code/core/Aspid/Design/css/styles.css')
             ->addCss('themes/bootstrap/Design/css/bootstrap.min.css')
             ->addJs('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', 1)
             ->addJs('themes/bootstrap/Design/js/bootstrap.min.js');

        $page->addChild('body', $body)
             ->addChild('head', $head);
        
        Aspid::getResponse()->setBody($page);
    }
    
    protected function _dispatch()
    {
        $controller = Aspid::getRoute()->getController();
        $action = Aspid::getRoute()->getAction();
        $content = $controller->$action();
        $pageView = Aspid::getResponse()->getBody();
        $pageView->getChild('body')->
            addChild('content', $content);
    }
    
    protected function _postDispatch()
    {
        Aspid::getResponse()->send();
    }
    
    
    public function defaultAction()
    {
        
    }
    
    public function notFoundAction()
    {
        echo '404';
    }
    
    /**
     * @param string $urlParsedActionName  
     * 
     * @return string
     */
    public function getAction($urlParsedActionName)
    {
        $finalActionName = $urlParsedActionName . 'Action';
        if (method_exists($this, $finalActionName))
        {
            Aspid::log($finalActionName . '() exists and returned', 'ACTION');
        }
        else
        {
            $finalActionName = 'defaultAction' ;
            Aspid::log($finalActionName . '() doesnt exists, defaultAction() returned', 'ACTION');
        }
        return $finalActionName;
    }
}
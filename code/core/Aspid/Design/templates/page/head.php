<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php foreach ($this->getCss() as $path): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $path ?>">
<?php endforeach ?> 
<?php foreach ($this->getJs() as $path): ?>
    <script src="<?php echo $path ?>"></script>
<?php endforeach ?>

<title>All Minsk apartments and services!</title>
</head>
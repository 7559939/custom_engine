<body>
<?php echo $this->renderChild('header'); ?>
<div class="container-fluid">
    <div class="row content">
        <?php echo $this->renderChild('menu'); ?>

        <div id="content" class="col-lg-8 col-md-10 col-sm-10 col-xs-12">
            <?php echo $this->renderChild('content'); ?>
        </div>
    </div>
</div>

<?php echo $this->renderChild('footer'); ?>
</body>
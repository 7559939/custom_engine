    <nav class="navbar navbar-inverse visible-xs">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="pull-left navbar-toggle" 
                  data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Logo</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo Aspid::getBaseUrl()?>">Main page</a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/oneroom"?>">1 room</a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/tworoom"?>">2 rooms</a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/threeroom"?>">3 rooms</a></li>
          </ul>
        </div>
      </div>
    </nav>


        <div class="col-sm-2 sidenav hidden-xs">
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="<?php echo Aspid::getBaseUrl()?>">Main page</a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/oneroom"?>">1 room </a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/tworoom"?>">2 rooms</a></li>
            <li><a href="<?php echo Aspid::getBaseUrl() . "/flat/default/threeroom"?>">3 rooms</a></li>
          </ul>
        </div>
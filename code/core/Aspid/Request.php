<?php

class Aspid_Request
{   
    /**
     * @var array 
     */
    protected $_path = array();
    /**
     * @var array 
     */
    protected $_parameters = array();
    /**
     * @var array 
     */
    protected $_get = array();
    /**
     * @var array 
     */
    protected $_post = array();
    
    public function __construct()
    {
        $this->_initializeRequest();
        $this->setPostGet();
    }
    
    protected function _initializeRequest()
    {
        $urlPartsArray = parse_url($_SERVER['REQUEST_URI']);
        $urlPartsArray = explode('/', (urldecode(trim($urlPartsArray['path'], '/'))));
        
        $urlMCAParts = array_shift($urlPartsArray);
        $this->_path['module'] = (empty($urlMCAParts)) ? 'default' : $urlMCAParts;
        
        $urlMCAParts = array_shift($urlPartsArray);
        $this->_path['controller'] = (empty($urlMCAParts)) ? 'default' : $urlMCAParts;
        
        $urlMCAParts = array_shift($urlPartsArray);
        $this->_path['action'] = (empty($urlMCAParts)) ? 'default' : $urlMCAParts;
        
        while (!empty($urlPartsArray))
        {
            $paramName = array_shift($urlPartsArray);
            $paramValue = array_shift($urlPartsArray);
            if (!empty($paramValue))
            {   
                $paramValue = (stristr($paramValue, '|')) ? (explode('|',$paramValue)) : $paramValue;
            }
            else
            {   
                $paramValue = true;
            }
            $this->_parameters[$paramName] = $paramValue;
        }
    }
    
    public function getPath($key = NULL, $default = NULL)
    {   
        if (empty($key))
        {
            return $this->_path;
        }
        else
        {
            return (!empty($this->_path[$key])) ?
                $this->_path[$key] :
                $default;
        }
    }

    public function getParameters($key = NULL, $default = NULL)
    {
        if (empty($key))
        {
            return $this->_parameters;
        }
        else
        {
            return (!empty($this->_parameters[$key])) ?
                $this->_parameters[$key] :
                $default;
        }
    }

    public function getGet($key = NULL, $default = NULL)
    {
        if (!empty($this->_get))
        {
            return (isset($key)) ?
                $this->_get[$key] :
                $this->_get;
        }
        else
        {
            return $default;
        }
    }
    
    public function getPost($key = NULL, $default = NULL)
    {
        if (!empty($this->_post))
        {
            return (isset($key)) ?
                $this->_post[$key] :
                $this->_post;
        }
        else
        {
            return $default;
        }
    }
    
    protected function setPostGet()
    {
        if (!empty($_GET)) 
        {
            $this->_get = $_GET;
        }
        if (!empty($_POST)) 
        {
            $this->_post = $_POST;
        }
    }
}

<?php

class Aspid_Route
{
    /**
     * @var array 
     */
    protected $_route = array();
    
    public function __construct()
    {
        $urlRequestedModule = Aspid::getRequest()->getPath('module');
        $urlRequestedController = Aspid::getRequest()->getPath('controller');
        $urlRequestedAction = Aspid::getRequest()->getPath('action');
        
        if ($urlRequestedModule == 'default')
        {
            $this->_route['module'] = Aspid::getModule(Aspid::getConfig('defaultModule'));
            Aspid::log('default module ' . Aspid::getConfig()['defaultModule'] . 
                    ' is found and returned, according to request', 'MODULE');
        }
        else
        {
            foreach (Aspid::getModules() as $key => $module)
            {
                $moduleFrontName = $module->getConfig('frontName');
                $isModuleEnabled = $module->getConfig('enabled');
                
                if ($moduleFrontName == Aspid::getRequest()->getPath('module'))
                {
                    Aspid::log("module $moduleFrontName found. Checking is enabled", 'MODULE');
                    if ($isModuleEnabled)
                    {
                        Aspid::log("module $moduleFrontName is enabled", 'MODULE');
                        $this->_route['module'] = Aspid::getModule($key);
                    }
                }
            }
        }
        
        if (empty($this->_route['module']))
        {
            Aspid::log("module $urlRequestedModule not found or not enabled. "
                    . "Returned base-controller, notFoundAction()", 'MODULE');
            $this->_route['controller'] = new Aspid_Controller;
            $this->_route['action'] = 'notFoundAction';
            return;
        }
        
       $this->_route['controller'] = $this->_route['module']->getController($urlRequestedController);
       $this->_route['action'] = $this->_route['controller']->getAction($urlRequestedAction);
    }
    
    /**
     * @return \Aspid_Module
     */
    public function getModule()
    {
        return $this->_route['module'];
    }
    
    /**
     * @return \Aspid_Controller
     */
    public function getController()
    {
        return $this->_route['controller'];
    }
    
    /**
     * @return string
     */
    public function getAction()
    {
        return $this->_route['action'];
    }
}

<?php

class Aspid_View_Page_Head extends Aspid_View
{
    protected $css = [];
    protected $js = [];
    
    public function __construct()
    {
        parent::__construct(SITE_ROOT . '/code/core/Aspid/Design/templates/page/head.php');
    }
    
    /**
     * @param string $path
     * @param boolean $external
     * @return $this
     */
    public function addJs($path, $external = null)
    {
        if ($external)
        {
            $this->js[] = $path;
        }
        
        else
        {
            $this->js[] = Aspid::getBaseUrl() . '/' . $path;
        }

        return $this;
    }
    
    /**
     * @param string
     *
     * @return $this
     */
    public function addCss($path)
    {
        $this->css[] = Aspid::getBaseUrl() . '/' . $path;

        return $this;
    }

    /**
     * @return array $css
     */
   public function getCss()
   {
       return $this->css;
   }
   
    /**
     * @return array $js
     */
   public function getJs()
   {
       return $this->js;
   }
}